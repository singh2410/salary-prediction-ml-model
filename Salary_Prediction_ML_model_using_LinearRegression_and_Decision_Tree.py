#!/usr/bin/env python
# coding: utf-8

# # Salary Prediction ML model using LinearRegression and Decision Tree
# #By- Aarush Kumar
# #Dated:May 22,2021

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


# In[2]:


df=pd.read_csv(r"/home/aarush100616/Downloads/Projects/Salary Prediction/data.csv")
df


# In[3]:


df.head(10)


# In[4]:


plt.scatter(df['YearsExperience'],df['Salary'])
plt.xlabel('Years of Experience')
plt.ylabel("Salary")
plt.show()


# In[5]:


X = df.iloc[:,:-1].values
y=df.iloc[:,-1].values


# In[6]:


X


# In[7]:


y


# In[8]:


from sklearn.model_selection import train_test_split


# In[9]:


X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=1/3,random_state=101)


# In[10]:


X_train


# In[11]:


X_test


# In[12]:


y_train


# In[13]:


y_test


# In[14]:


# LinearRegression
from sklearn.linear_model import LinearRegression
LR=LinearRegression()


# In[15]:


LR.fit(X_train,y_train)


# In[16]:


y_pred_LR=LR.predict(X_test)


# In[17]:


X_test


# In[18]:


y_test


# In[19]:


y_pred_LR


# In[20]:


diff_LR=y_test-y_pred_LR
res_df=pd.concat([pd.Series(y_pred_LR),pd.Series(y_test),pd.Series(diff_LR)],axis=1)
res_df.columns=['Prediction','Original Data','Diff']


# In[21]:


res_df


# In[22]:


plt.scatter(X_train,y_train,color='blue')
plt.plot(X_train,LR.predict(X_train),color='red')
plt.title('Salary vs Experience')
plt.xlabel('Years of Experience')
plt.ylabel("Salary")
plt.show()


# In[23]:


plt.scatter(X_test,y_test,color='blue')
plt.plot(X_train,LR.predict(X_train),color='red')
plt.title('Salary vs Experience (Test Set)')
plt.xlabel('Years of Experience')
plt.ylabel("Salary")
plt.show()


# In[24]:


# Metrics
from sklearn import metrics
rmse= np.sqrt(metrics.mean_squared_error(y_test,y_pred_LR))
R2=metrics.r2_score(y_test,y_pred_LR)
rmse


# In[26]:


R2*100


# In[27]:


# Decision Tree
from sklearn.tree import DecisionTreeRegressor
DT=DecisionTreeRegressor()
DT.fit(X_train,y_train)


# In[28]:


y_pred_dt=DT.predict(X_test)


# In[29]:


y_pred_dt


# In[30]:


y_test


# In[31]:


diff_DT=y_test-y_pred_dt


# In[32]:


res_dt=pd.concat([pd.Series(y_pred_dt),pd.Series(y_test),pd.Series(diff_DT)],axis=1)
res_dt.columns=['Prediction','Original Data','Diff']
res_dt


# In[33]:


X_test


# In[34]:


from sklearn import metrics
rmse= np.sqrt(metrics.mean_squared_error(y_test,y_pred_dt))
R2=metrics.r2_score(y_test,y_pred_dt)
rmse


# In[35]:


R2*100


# In[36]:


from sklearn import tree
text_representation=tree.export_text(DT)
print(text_representation)


# In[37]:


fig=plt.figure(figsize=(25,20))
_=tree.plot_tree(DT,feature_names=df['YearsExperience'],filled=True)

