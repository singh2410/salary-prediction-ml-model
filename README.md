# Salary Prediction ML model
# By- Aarush Kumar

Salary prediction model using various ML algorithms as LinearRegression & Decision Tree.
First let’s look at the dataset.It has 2 columns — “Years of Experience” and “Salary” for 30 employees in a company. So in this example, we will train a Simple Linear Regression model to learn the correlation between the number of years of experience of each employee and their respective salary. Once the model is trained, we will be able to do some sample predictions.
Steps followed are as:
1.Load the Dataset.
2.Split dataset into training set and test set.
3.Fit Simple Linear Regression model to training set.
4.Visualizing the training set.
5.Visualizing the testing set.
6.Predict the test set.
7.Print the DecisionTree.
Thankyou!
